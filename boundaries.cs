namespace assignment_seventh;
using System.Drawing;
using System.Drawing.Imaging;

public class boundaries
{
    public static void removeBoundies(Bitmap bmp)
    {
        
        BitmapData bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height),
            ImageLockMode.ReadWrite, bmp.PixelFormat);

        unsafe
        {
            byte * ptr = (byte *)bmpData.Scan0;
            int bytesPerPixel = Bitmap.GetPixelFormatSize(bmp.PixelFormat) / 8;
            int heightInPixels = bmpData.Height;
            int widthInBytes = bmpData.Width * bytesPerPixel;
            for (int y = 0; y < heightInPixels; y++)
            {
                byte * row = ptr + (y * bmpData.Stride);
                for (int x = 0; x < widthInBytes; x += bytesPerPixel)
                {
                    if (row[x] == 255 && row[x + 1] == 255 && row[x + 2] == 255) 
                    {
                        row[x] = 0; 
                        row[x + 1] = 0;
                        row[x + 2] = 0;
                    }
                }
            }
        }

        bmp.UnlockBits(bmpData);

        bmp.Save("../../../Images/newImage.jpg");
    }
    
    

    public static void binaryImage(Bitmap binaryImage)
    {
        
        

        Rectangle rect = new Rectangle(0, 0, binaryImage.Width, binaryImage.Height);
        BitmapData binaryImageData = binaryImage.LockBits(rect, ImageLockMode.ReadWrite, binaryImage.PixelFormat);

        unsafe
        {
            byte* binaryImagePtr = (byte*)binaryImageData.Scan0;
            int height = binaryImage.Height;
            int width = binaryImage.Width;
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (i == 0 || i == height - 1 || j == 0 || j == width - 1)
                    {
                        binaryImagePtr[i * binaryImageData.Stride + j] = 0;
                    }
                }
            }
        }

        binaryImage.UnlockBits(binaryImageData);
        binaryImage.Save("../../../Images/t.bmp");


    }

}